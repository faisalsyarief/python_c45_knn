# FlaskApp

Simple application with authentication and CRUD functionality using the Python Flask micro-framework

## Installation

To use this template, your computer needs:

- [Python 2 or 3](https://python.org)
- [Pip Package Manager](https://pypi.python.org/pypi)
- [MySQL for database / XAMPP 5](https://www.apachefriends.org/index.html)
- [Database kelulusan.sql](https://gitlab.com/ichaltkj96/python_c45_knn/-/blob/master/kelulusan.sql)

### Running the app

```bash
python app.py
```

